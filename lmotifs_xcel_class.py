#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
motifs_excel_class (motifs 11)
23 september 2008
"""
#
import xlwt
from lmotifs_common import AMINO_ACIDS


class ExcelMotif(xlwt.Workbook):
    def __init__(self):
        xlwt.Workbook.__init__(self)

    def sheet_motifs(self, name, my_dict):
        """Motifs sheet."""
        ws = self.add_sheet(name)
        row = 0
        for item in my_dict:
            for motif in my_dict[item]:
                ws.write(row, 0, item.strip())  # strip?
                ws.write(row, 1, motif)
                row += 1

    def sheet_freqs(self, name, freqs, head):
        """Frequency sheet."""
        style = xlwt.XFStyle()
        style.num_format_str = "0"
        ws = self.add_sheet(name)
        title = " | ".join(head)
        ws.write(0, 0, title)
        ws.write(1, 0, "pos")
        for number in range(len(freqs[0])):
            ws.write(1, number + 1, number + 1)
        for idx, aa in enumerate(AMINO_ACIDS):
            ws.write(idx + 2, 0, aa)
            for pos, freq in enumerate(freqs[idx]):
                # xlwt version fails with numpy floats and int32
                # must convert to python int or float
                ws.write(idx + 2, pos + 1, round(float(freq), 1), style)

    def sheet_graph(self, name, graph, head):
        """Motif graph sheet."""
        ws = self.add_sheet(name)
        title = " | ".join(head)
        ws.write(0, 0, title)
        ws.insert_bitmap(graph, 1, 0)

    def xls_save(self, arch):
        """Save Excel file."""
        output_file = arch + "out.xls"
        self.save(output_file)


if __name__ == "__main__":
    xlm = ExcelMotif()
