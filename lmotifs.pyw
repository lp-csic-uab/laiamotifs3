#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
lmotifs
1 april 2012
"""
#
import wx
import os
from numpy import array
from commons.mass import prob_aa_vert as prob_aa
from lmotifs_class import Motifs
from lmotifs_graph_class import GraphMotif
from lmotifs_xcel_class import ExcelMotif
from lmotifs_common import AMINO_ACIDS, TEXTFROMFILE, TEXTDEFAULT
from lmotifs_graph_viewer import Viewer
#
#
# '$' is considered here an empty place (No aa)
# it is given an average frequency value
# >>> from commons.mass import prob_aa
# >>> np.mean(prob_aa.values())
# 0.0470
prob_aa['$'] = 0.05
#
#
class MyMotif(Motifs):
    """"""
    def __init__(self, parent):
        Motifs.__init__(self, parent)
        self.graph_name = "_graph.bmp"
        self.my_dict = dict()
        self.file = None
        self.anchor = None
        self.size = None
        self.pattern = None
        self.pos = 0
        self.prob_aa = None
        self.raw_freqs = None
        self.notes = None
        self.freq_file = False
        self.freq_type = "Unknown"
        self.heading = []
        self.freqs = []
        self.graphs = []
        self.no_nested = False
        self.two_anchors = False
        self.graph_path = None
        self.dict_prob_aa = prob_aa

        self.init()

    def init(self):
        """"""
        try:
            for line in open("frequencies.txt"):
                if (line.strip() == "") or line.startswith("#"):
                    continue
                [key, value] = line.split(':')
                key = key.strip()
                value = value.strip()
                if key == "name":
                    self.freq_type = value
                    continue
                value = float(value)
                self.dict_prob_aa[key] = value
                self.freq_file = True
        except IOError:
            self.freq_file = False

        if self.freq_file:
            self.tc_out.WriteText(TEXTFROMFILE)
        else:
            self.freq_type = "vertebrates"
            self.tc_out.WriteText(TEXTDEFAULT)

        self.heading.append("corrected for natural frequencies in %s"
                            % self.freq_type)
        prob_list = [[self.dict_prob_aa[item]] for item in AMINO_ACIDS]
        self.prob_aa = array(prob_list)

    def fbbCallback(self, evt):
        """"""
        self.file = evt.GetString()

    def doit(self, evt):
        """"""
        self.heading = self.heading[0:1]
        if not self.file or not os.path.exists(self.file):
            graph = GraphMotif(AMINO_ACIDS)
            viewer = Viewer(graph.base, None, -1, "")
            viewer.Show()
            return
        #
        self.anchor = self.tc_anchor.GetValue()
        self.size = int(self.sp_size.GetValue())
        self.pattern = self.tc_pattern.GetValue()
        self.pos = int(self.sp_pos.GetValue())
        self.no_nested = self.cbx_no_nested.IsChecked()
        self.two_anchors = self.cbx_two_anchors.IsChecked()
        #
        self.search_motifs()
        if self.no_nested:
            self.get_no_nested()
            self.heading.append("Sequences No Nested")
        if self.two_anchors:
            self.get_two_anchors()
            self.heading.append("Anchor 2 with %s in pos %s"
                                % (self.pattern, self.pos))
        self.calc_freqs()
        #
        self.write_to_gui()
        self.graph_motifs()
        self.write_to_excel()

    def write_to_gui(self):
        """"""
        self.tc_out.Clear()
        self.write_motifs()
        self.write_freqs()
        self.write_notes()

    def get_no_nested(self):
        """"""
        total = []
        for item in self.my_dict:
            partial = []
            for sec in self.my_dict[item]:
                if sec in total:
                    continue
                else:
                    partial.append(sec)
                    total.append(sec)

            self.my_dict[item] = partial

    def get_two_anchors(self):
        """"""
        # print "in two anchors"
        for item in self.my_dict:
            partial = []
            for sec in self.my_dict[item]:
                if sec[self.pos - 1] in self.pattern:
                    partial.append(sec)

            self.my_dict[item] = partial

    def get_percent_freqs(self, freqs):
        """Frequencies in percentage"""
        sum_array = freqs.sum(0)
        return freqs.astype(float) * 100 / sum_array

    def get_corrected_freqs(self, freqs):
        """Correct AA frequencies with naturally expected frequencies

        Result is given in percentage
        """
        corrected_freqs = freqs / self.prob_aa
        return self.get_percent_freqs(corrected_freqs)

    def search_motifs(self):
        """"""
        for item in open(self.file):
            # the file can have commented lines
            if item.startswith("#"):
                continue
            item = item.strip()
            peptides = []
            for anchor in self.anchor:
                index = -1
                while True:
                    index = item.find(anchor, index + 1)
                    if index == -1:
                        break
                    if len(item[index:]) >= self.size:
                        peptides.append(item[index:index + self.size])
                    else:
                        break
            self.my_dict[item] = peptides

    def calc_freqs(self):
        """"""
        self.freqs = []
        self.notes = []
        aa_number = len(AMINO_ACIDS)
        alist = [0] * self.size * aa_number
        aa_ranking = dict()
        for item in AMINO_ACIDS:
            aa_ranking[item] = AMINO_ACIDS.index(item)

        self.raw_freqs = array(alist).reshape(aa_number, self.size)

        for item in self.my_dict:
            for motif in self.my_dict[item]:
                idx = 0
                for amino in motif:
                    try:
                        pos = aa_ranking[amino]
                    except KeyError:
                        self.notes.append(
                            "\nAmino acid < %s > unknown. Assigned to 'X'"
                            % amino)
                        pos = aa_ranking["X"]

                    self.raw_freqs[pos][idx] += 1
                    idx += 1

        self.freqs.append((self.raw_freqs,
                           "frequencies", self.heading[1:]))
        self.freqs.append((self.get_percent_freqs(self.raw_freqs),
                           "percent_freqs", self.heading[1:]))
        self.freqs.append((self.get_corrected_freqs(self.raw_freqs),
                           "correct_freqs", self.heading))

    def graph_motifs(self):
        """"""
        self.graphs = []
        for frequencies, name, heading in self.freqs:
            if name == "frequencies":
                continue
            graph = GraphMotif(AMINO_ACIDS)
            graph.graph(frequencies)
            dir_path, filename = os.path.split(self.file)
            filename = filename.replace('.', '_')
            graph_name = filename + '_' + name + self.graph_name
            path = os.path.join(dir_path, graph_name)
            graph.save(path)
            self.graphs.append((name, path, heading))
            viewer = Viewer(path, None, -1, name)
            viewer.Show()

    def write_to_excel(self):
        """Write data and graphs into different sheets of an Excel document"""
        motif = ExcelMotif()
        motif.sheet_motifs("motifs", self.my_dict)
        for freq, name, heading in self.freqs:
            motif.sheet_freqs(name, freq, heading)
        for name, path, heading in self.graphs:
            motif.sheet_graph(name + "_graph", path, heading)

        motif.xls_save(self.file)

    def write_motifs(self):
        """Write motifs in TextCtrl."""
        for item in self.my_dict:
            text = "%s\n%s\n\n" % (item, self.my_dict[item])
            self.tc_out.WriteText(text)

    def write_freqs(self):
        """Write frequency tables in TextCtrl."""
        for freqs, name, heading in self.freqs:
            text = "\n%s\n" % name
            for item in heading:
                text += "%s\n" % item
            text += "pos -> "
            for number in range(self.size):
                text += "%6s  " % (number + 1)
            self.tc_out.WriteText(text + '\n')

            for aa_idx, amino in enumerate(AMINO_ACIDS):
                text = " %s   -> " % amino
                for pos in range(self.size):
                    text += "%5.1f  " % (freqs[aa_idx][pos])
                self.tc_out.WriteText(text + '\n')

    def write_notes(self):
        """Write comments in TextCtrl."""
        if self.notes:
            notes = ''.join(self.notes)
            self.tc_out.WriteText(notes)

    def open(self, evt):
        """Open result excel file."""
        if self.file:
            os.startfile(self.file + "out.xls")


if __name__ == '__main__':

    App = wx.App()
    fr = MyMotif(None)
    # noinspection PyUnresolvedReferences
    fr.Show()
    # noinspection PyUnresolvedReferences
    App.MainLoop()
