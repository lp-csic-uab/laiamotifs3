
---

**WARNING!**: This is the *Old* source-code repository for LaiaMotifs3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/laiamotifs3/) located at https://sourceforge.net/p/lp-csic-uab/laiamotifs3/**  

---  
  
![https://lh5.googleusercontent.com/-ZHFdJH3cnok/Spzj3Jy4D6I/AAAAAAAAAC4/FBaI6IXmCmk/s800/Laias-motifs-logo.png](https://lh5.googleusercontent.com/-ZHFdJH3cnok/Spzj3Jy4D6I/AAAAAAAAAC4/FBaI6IXmCmk/s800/Laias-motifs-logo.png)


---

**WARNING!**: This is the *Old* source-code repository for LaiaMotifs3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/laiamotifs3/) located at https://sourceforge.net/p/lp-csic-uab/laiamotifs3/**  

---  
  

**Table Of Contents:**

[TOC]

#### Description

This utility helps in the search for [MHC-like motifs](http://www.jimmunol.org/cgi/content/full/181/1/795) in collections of peptide sequences obtained from proteomic analyses.
**LAIA motifs** program will search for sequence motifs characterized by an anchor position defined by the presence of one or several specific amino acids and will determine the frequency of the different amino acids in each position in the sequence to the right (C-terminal) of the anchor.

Amino acids defining the anchor and the motif size are selectable. An additional position  in the peptide (relative to the anchor) can be defined as a second anchor in order to search for more restricted sequence patterns.

![http://lh5.ggpht.com/_ty9pOjnnHDA/Spzj3GvALOI/AAAAAAAAAC0/wYxPWKgCeR8/Laias-motifs-GUI.png](http://lh5.ggpht.com/_ty9pOjnnHDA/Spzj3GvALOI/AAAAAAAAAC0/wYxPWKgCeR8/Laias-motifs-GUI.png)

Results are displayed graphically and stored in a MS Excel file in the form of frequency tables (absolute and normalized values).

Graphics are also embedded in the Excel document for convenience.

![http://lh3.ggpht.com/_ty9pOjnnHDA/Spzj3crp0bI/AAAAAAAAAC8/dIVCihGbtTo/mhc_txt_percent_frecs_graph.png](http://lh3.ggpht.com/_ty9pOjnnHDA/Spzj3crp0bI/AAAAAAAAAC8/dIVCihGbtTo/mhc_txt_percent_frecs_graph.png)

The program was coded in [Python](http://www.python.org) (Life is short, thanks [Guido](http://www.python.org/~guido/)), and uses [wxPython](http://www.wxpython.org/) for the GUI. The GUI was build with [wxGlade](http://wxglade.sourceforge.net/) and the installer was prepared with [py2exe](http://www.py2exe.org/) and [Inno-Setup](http://www.jrsoftware.org/isinfo.php).

This program has been written by Joaquin Abian ([LP CSIC/UAB](http://proteomica.uab.cat/)) in collaboration with Laia Muix� ([IBB](http://ibb.uab.cat/)) ([Autonomous University of Barcelona](http://www.uab.cat/), Catalonia, Spain)


#### Installation

##### From Installer

  1. Download the **windows installer** provided in the [Laia Motifs project download page](https://bitbucket.org/lp-csic-uab/laiamotifs/downloads).
  1. Double click on it and follow the Setup Wizard.
  1. That is!

##### From Source

  1. Install Python and third party software indicated in [Dependencies](#markdown-header-source-dependencies).
  1. Download mhcLAIAmotifs source from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/laiamotifs/src).
  1. Download commons source code from its [Mercurial Source Repository](http://code.google.com/p/lp-csic-uab/source/browse/?repo=commons).
  1. Copy the folder anywhere in your path.

###### _Source Dependencies:_

  * [Python](http://www.python.org) 2.6 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 2.8.9.2 - 2.8.10.0
  * [Numpy](http://numpy.scipy.org/) 1.2.1 - 1.4.1
  * [PIL](http://www.pythonware.com/products/pil/) 1.1.6
  * [xlwt](http://pypi.python.org/pypi/xlwt) 0.7
  * [commons](http://code.google.com/p/lp-csic-uab/source/browse/?repo=commons) (from LP CSIC/UAB GoogleCode [repository](http://code.google.com/p/lp-csic-uab/source/browse/?repo=commons))

Third-party program versions correspond to those used for the installer available here. Lower versions have not been tested, although they may also be fine.

#### Usage

  1. Use the **_Browse_** button to find and load a [Sequences Input File](#markdown-header-the-sequences-input-file) (a text file containing peptide sequences, one per line, single letter code).
  1. Set **_Anchor AAs_**, the amino acids  allowed in the anchor position (ex. **FYW** for MHC class-I).
  1. Set **_Size_**, the peptide motifs length (ex. **9** for MHC class-I).
  1. Check _**Non Nested**_ if you want to select only non-nested sequences.
  1. Check _**Two Anchors**_ if you want to select a subset of sequences with selected amino acids in a second position and set the allowed amino acids (**_Pattern AAs_**) and the position (**_pos_**).
  1. Run **_Search_**. Frequency data is shown in the Text window; and 2 image windows are opened, with the graphical representation of amino-acid frequencies. Moreover, the full data set is automatically stored in a MS Excel file (.xls)
  1. Press the **_Save_** button located in each image window if you want to keep the images produced
  1. Press **_Open XlS_** to open the MS Excel file and look at the full data set produced including images

Three frequency tables can be found in the MS Excel document:

  * Absolute counts for each amino acid in each position.
  * Normalized -percentage- frequencies.
  * Corrected frequencies that take into account the expected frequency for each amino acid in a given proteome.

To calculate the corrected frequencies, frequencies for [vertebrates](http://www.tiem.utk.edu/bioed/webmodules/aminoacid.htm) are used by default. If you want to change it, see [How To Set Specific AA Frequencies](#markdown-header-how-to-set-specific-aa-frequencies) below.

##### The Sequences Input File

Laia Motifs analyses peptide sequence sets that are provided in an input text file, one sequence per line, single letter code.
Comments can be inserted by commenting the line with the tag #.

A test sequence file mhc.txt is provided in the test folder.

##### How To Set Specific AA Frequencies

Laia Motif corrects the observed frequencies of aminoacids with the naturally observed frequencies in vertebrates. This default frequency data is taken from:
http://www.tiem.utk.edu/bioed/webmodules/aminoacid.htm

These frequencies can be modified by editing the **_frecuencias.txt_** file:

  * To use other frequencies create/edit the _frecuencias.txt_ files. Each line of this text file must be of the type **aminoacid:frecuency**:
```
C:0.110
A:0.074
```
  * Use the tag **name** at the first line in the file to indicate the data source:
```
name:frog
```
  * Start any line with the tag **#** for comments or to hide the data from the aminoacid in this line:
```
#This is a comment...
```
  * If an aminoacid is not indicated in the file or it is hidden (commented out), its default frequency in vertebrates will be used.

#### Download

You can download the last version of **MHC LAIA motifs** [here](https://bitbucket.org/lp-csic-uab/laiamotifs/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/laiamotifs/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program.

#### Features and Change-Log

1.00 september 2011

  * Initiate this record.
  * Represent Leu/ILe with the correct code J (before was X). Use X exclusively for unknown aminoacids.

#### To-Do

  * Make use of Viewer visible with... additional button?

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)


---

**WARNING!**: This is the *Old* source-code repository for LaiaMotifs3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/laiamotifs3/) located at https://sourceforge.net/p/lp-csic-uab/laiamotifs3/**  

---  
  
