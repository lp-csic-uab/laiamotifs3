#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
"""
lmotifs_class (motifs 11)
10 january 2009
"""
#
import wx
from commons.iconic import Iconic
from lmotifs_common import ICON
from lmotifs_baseclass import BaseMotifs
#
#
#noinspection PyUnusedLocal
class Motifs(BaseMotifs, Iconic):
    def __init__(self, *args, **kwds):
        BaseMotifs.__init__(self, *args, **kwds)
        Iconic.__init__(self, icon=ICON)

        self.Bind(wx.EVT_BUTTON, self.doit, self.bt_run)
        self.Bind(wx.EVT_BUTTON, self.open, self.bt_xls)
        self.Bind(wx.EVT_CLOSE, self.OnClose)

        title = "%10s%45s%40s" % ('LP CSIC/UAB', 'LAIAs_MOTIFS', 'vs 1.1')
        self.SetTitle(title)

        for item in [self.lb_31, self.tc_anchor]:
            item.SetToolTip("Specific Amino acids in anchor position 1")

        for item in [self.lb_32, self.sp_size]:
            item.SetToolTip("Length of the motif")

        for item in [self.lb_41, self.tc_pattern]:
            item.SetToolTip("Second anchor position."
                                  "Specific Amino acids in position 'Pos'")

        for item in [self.lb_42, self.sp_pos]:
            item.SetToolTip("Position of the second anchor in the"
                                  "sequence. First anchor is position 1")

        self.cbx_no_nested.SetToolTip(
            "Check to overlook nested sequences")
        self.cbx_two_anchors.SetToolTip(
            "Check to require a second anchor in position"
            " 'Pos' defined by 'PATTERN AAs'")

        self.bt_xls.SetToolTip("Opens the results excel file")

    def fbbCallback(self, evt):
        pass

    def doit(self, evt):
        pass

    def open(self, evt):
        pass

    def OnClose(self, evt):
        self.Destroy()


class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        motifs = Motifs(None, -1, "")
        self.SetTopWindow(motifs)
        motifs.Show()
        return 1


if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
