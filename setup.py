# exWx/setup.py
#
## python setup.py py2exe
#
from distutils.core import setup
from lmotifs_get_images import get_images
#noinspection PyUnresolvedReferences
import py2exe
#
####################################
PyAppName = "lmotifs.pyw"
AppName = "LaiaMotifs"
AppVers = "1.1"
Icon = 'images//LaiaMotifs48.ico'
####################################
#
setup(
    windows = [
              {'script': PyAppName,
               'icon_resources':[(0, Icon)],
               'dest_base' : "%s %s" %(AppName, AppVers),
               'version' : AppVers,
               'company_name' : "JoaquinAbian",
               'copyright' : "No Copyrights",
               'name' : AppName
              }
              ],

    options = {
              'py2exe': {
                        #'packages' :    [],
                        #'includes':     [],
                        'excludes':     ['matplotlib', 'email', 'testing', 'fft',
                                         'Tkconstants','Tkinter', 'tcl'
                                        ],
                        'ignores':       ['wxmsw26uh_vc.dll'],
                        'dll_excludes': ['libgdk_pixbuf-2.0-0.dll',
                                         'libgdk-win32-2.0-0.dll',
                                         'libgobject-2.0-0.dll'
                                        ],
                        'compressed': 1,
                        'optimize':2,
                        'bundle_files': 1
                        }
              },
    zipfile = None,
    data_files = [("", ["INSTALL.txt",   # Last page of installer
                        "README.txt",    # program README and help files
                        "LICENCE.txt",
                        "frequencies.txt",
                        "msvcp90.dll",
                        "C:\\Python27/Lib/site-packages/wx-2.8-msw-unicode/wx/gdiplus.dll",
                        "C:\\Python27/lib/site-packages/numpy/core/umath.pyd"
                        ]
                  ),
                  ("test", ["test\\mhc.txt"
                        ]
                  )
                 ] + [get_images()]
    )
