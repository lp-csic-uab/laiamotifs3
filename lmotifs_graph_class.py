#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
motifs_graph_class (motifs 11)
23 september 2008
"""
#
try:
    import Image
except ImportError:
    from PIL import Image
import os
from numpy import array
import _thread as thread


class GraphMotif:
    def __init__(self, amino_acids):
        self.aminos = amino_acids
        self.norm_freqs = []
        self.motif_size = None
        self.img_basename = None
        self.img_base = None
        self.img = dict()
        dir_images = os.path.dirname(__file__)
        self.base = os.path.join(dir_images, r'AAS/base.bmp')

        self.read()

    def read(self):
        """"""
        self.img_basename = os.path.basename(self.base).split(".")[0]
        self.img_base = Image.open(self.base)
        img_dir = os.path.dirname(self.base)
        for aa in self.aminos:
            if aa in "syt":
                aa_name = "p" + aa.upper()
            else:
                aa_name = aa

            name = img_dir + os.sep + aa_name + ".bmp"

            # in case there is no image for a given AA...
            try:
                self.img[aa] = Image.open(name)
            except IOError:
                name = img_dir + os.sep + "X.bmp"
                self.img[aa] = Image.open(name)

    def graph(self, frequencies):
        """"""
        self.motif_size = len(frequencies[0])
        # noinspection PyNoneFunctionAssignment
        freqs_ = array(frequencies).astype(float)
        sum_ = freqs_.sum(0)
        norm_freq = freqs_ * 4 / sum_
        # noinspection PyUnresolvedReferences
        self.norm_freqs = norm_freq.swapaxes(0, 1)

        self.make_img()

    def make_img(self):
        """"""
        factor = 50
        for idx in range(self.motif_size):
            pares = [(fr, item) for item, fr
                     in zip(self.aminos, self.norm_freqs[idx])]
            spares = sorted(pares, reverse=True)
            self.img_base = self.img_base.resize((factor * self.motif_size,
                                                  200))

            xpos = factor * idx
            ypos = 0
            for fr, item in spares:
                new_size = int(factor * fr)
                # new PIL versions do not create images of size 0
                if new_size == 0:
                    continue
                new_img = self.img[item].resize((factor, new_size))
                self.img_base.paste(new_img, (xpos, ypos,
                                              xpos + factor,
                                              ypos + new_size))
                ypos += new_size

    def show(self):
        thread.start_new_thread(self.show_threaded, (self.img_base,))

    # noinspection PyMethodMayBeStatic
    def show_threaded(self, img):
        img.show()

    def save(self, name):
        self.img_base.save(name)


if __name__ == "__main__":

    freqs = [
            [1, 10, 6, 2, 5, 3, 4, 8, 0],
            [4, 5, 6, 0, 3, 0, 10, 4, 1],
            [1, 10, 2, 2, 2, 8, 1, 0, 0],
            [10, 8, 1, 2, 1, 0, 5, 3, 4],
            [3, 2, 10, 5, 1, 10, 4, 3, 2],
            [1, 8, 5, 20, 1, 0, 5, 3, 4],
            [5, 4, 1, 5, 10, 7, 8, 9, 3],
            [10, 5, 1, 4, 1, 0, 8, 3, 2],
    ]
#
    size = 9
    AMINOS = "ARSYWLtJ"
    motif = GraphMotif(AMINOS)
    motif.graph(freqs)
    motif.show()
    exit_ = input('press enter to exit')
