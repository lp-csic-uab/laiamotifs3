#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
"""
lmotifs_common (motifs 11)
15 september 2011
"""
#
# $ stands for no aminoacid (sequence n-, c- extremes)
AMINO_ACIDS = "ACDEFGHIKLMNPQRSTVWYstyJX$"

#note this moved to commons
##frequency in vertebrates
##http://www.tiem.utk.edu/bioed//webmodules/aminoacid.htm
#prob_aa =  {'G':0.074, 'A':0.074, 'S':0.081,
#            'P':0.050, 'V':0.068, 'T':0.062,
#            'C':0.033, 'J':0.057, 'N':0.044,
#            'D':0.059, 'Q':0.037, 'K':0.072,
#            'E':0.058, 'M':0.018, 'H':0.029,
#            'm':0.018, 'F':0.040, 'R':0.042,
#            'c':0.033, 'Y':0.033, 's':0.081,
#            't':0.062, 'W':0.013, 'y':0.033,
#            'L':0.076, 'I':0.038, 'X':0.001
#            }

TEXTFROMFILE = ("""

                         Frequency data read from "frequencies.txt" file.

                             For default frequencies (Vertebrates),
                             change the name of the file to any other or
                             use a # tag to comment out file lines
                        """
                        )

TEXTDEFAULT = ("""
                                 Amino acid Frequency data for Vertebrates
                    "http://www.tiem.utk.edu/bioed//webmodules/aminoacid.htm"

                   To use other frequencies create a text file "frequencies.txt"
                         with lines of the type C:0.11 (amino_acid:frequency).
                             Use the tag 'name' at the first line in the file
                             to indicate the data source (pe. name:frog).
  If an amino acid is not indicated in the file its default frequency in vertebrates will be used.
      Start any line with the tag # for comments or to hide the data from a given amino acid"""
                        )

ICON = """AAABAAEAICAAAAEACACoCAAAFgAAACgAAAAgAAAAQAAAAAEACAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAkqb7AGWA+ACNovsAEjblADFAwwA6QrgANEG/ACY8zgAROOoAIzzSAE5HngBcS4wA
VEmYAEFErgARN+YAEjjqACI71ABuT3YAhVVaAJ9cOQCRWEoAKT3LABY97AAAzf8AJDzRAItXUQCr
XyoAm1o9AHRQbgAtPscAAAD/AAB2/wAAq/8AANj/ABM24wBTSZgAY02EAHZRbQBqTnsAFzfdAEJj
+ABu//8AAJ7/AACs/wAYN9wAQ0WsAE9InQBIRqYAN0K7ABM98AAb/f8AAP7/ABE67wAROeoADznv
AEFl9wAA/f8AAPn/ALjF/QDR2v4AfOf/AAD8/wAA//8AEML/ACSu/wABzf8AAL3/AACg/wAAzv8A
AHT/AADX/wAAvP8AAJ//AADM/wAAnf8Ak8r/AHDV/wBy7P8Acv//AHL7/wBy2f8Adsz/AHLj/wBy
/f8Acv7/AG/j/wCG0v8AAID/AAC1/wAA3f8AAPj/AAC6/wAAgf8AAoL/AAm3/wAJ3v8AA///AAP8
/wAIvP8AJK//ACjZ/wAZ+fwAAe/7ACnc/wAjsP8AAyzNAAMuxgBHYcgAjIyMAHJxcQBSUE8AQUA/
ABUVFwABAwoAISAfACAfHgAdHRsAUlJSAFBQUAB/f38Anp6eANDQ0ACcnJwANjY2AHJycgDAv74A
wcDAAMLCwQA0RowANESCALW1tACVlZUAOTk5ABYWFgAJCQkAFhcYAAECCQAhISEAICAgACIiIgAn
JycA0dHRAKioqACnp6cAa2trAGxsbABtbW0AHSZGAB0lQQCDg4MAwcHBAAMtzwBIYcYAXnG9ACFA
uwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/
//8AAAAAAAAAAAAAapoAAAAAAAAAAABqmwAAAAAAAAAAAAAAAAAAAAAAAABqmQAAAAAAAAAAAGpq
AAAAAAAAAAAAAAAAAAAAj5CRkpOUlZaQlwAAAAAAapgAAAAAAAAAAAAAAAAAent8fX5/gIGCg4SF
hgAAAIeIiYqLjAAAAI2OAAAAAAAAAAAAAABqbAAAAAAAbW5vcHFyc3R1dnd4eQAAAAAAAAAAAAAA
AGpqAAAAAAAAAAAAamsAAAAAAAAAAAAAAAAAAAAAAF5fYGFiYwAAAAAAZGVmZ2hpAAAAAAAAAAAA
AAAAAAAfXVlaP1tcHwAAAB9ERTk5GCsfAAAAAAAAAAAAAAAAAB9dWVo/W1wfAAAAH0RFOTkYKx8A
AAAAAAAAAAAAAAAAH11ZWj9bXB8AAAAfREU5ORgrHwAAAAAAAAAAAAAAAAAfXVlaP1tcHwAAAB9E
RTk5GCsfAAAAAAAAAAAAAAAAAB9dWVo/W1wfAAAAH0RFOTkYKx8AAAAAAAAAAAAAAAAAH11ZWj9b
XB8AAAAfREU5ORgrHwAAAAAAAAAAAAAAAAAfXVlaP1tcHwAAAB9ERTk5GCsfAAAAAAAAAAAAAAAA
AB9dWVo/W1wfAAAAH0RFOTkYKx8AAAAAAAAAAAAAAAAAH11ZWj9bXB8AAAAfREU5ORgrHwAAAAAA
AAAAAAAAAAAfXVlaP1tcHwAAAB9ERTk5GCsfAAAAAAAAAAAAAAAAAB9YWVo/W1wfAAAAH0RFOTkY
Kx8AAAAAAAAAAAAAAAAAAExNTk9QUQAAAAAAUlNUVVZXAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAABGIUc0OkgAAAAAAEkYOTlKSwAAAAAAAAAAAAAAAAAAHx8s
IjQ6Qx8AAAAfREU5ORgrHwAAAAAAAAAAAAAAAAAfHywiPzpAAAAAAABBQjk5GCsfAAAAAAAAAAAA
AAAAAB8gLCI0OgAAOzY8AAA9PjkYKx8AAAAAAAAAAAAAAAAAHyAsIjQAABA1Njc4AAAAORgrHwAA
AAAAAAAAAAAAAAAfICwiAAAQLS4vMDEyAAAzGCsfAAAAAAAAAAAAAAAAAB8gISIAECMkJSYnJCgp
ACoYKx8AAAAAAAAAAAAAAAAAABgYAAAQGRIaGxwdHhAAABgYAAAAAAAAAAAAAAAAAAAAAAAAABAR
EhMUFRIWFwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkKCwwNDg8AAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAQFBgcIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAECAwAAAAAAAAAAAAAAAAAA
AP+f8///n/P/+AHz/+AAAAP/nwAH/5/z//4HwP/8A4B//AOAf/wDgH/8A4B//AOAf/wDgH/8A4B/
/AOAf/wDgH/8A4B//AOAf/4HwP///////gfA//wDgH/8B8B//Axgf/wYOH/8MBh//CAIf/5gDP//
4A////Af///4P////H//"""
