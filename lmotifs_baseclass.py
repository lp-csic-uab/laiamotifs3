#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# generated by wxGlade 0.6.2 on Fri Jan 09 19:11:51 2009

import wx
import  wx.lib.filebrowsebutton as filebrowse
# begin wxGlade: extracode
# end wxGlade

#noinspection PyArgumentEqualDefault
class BaseMotifs(wx.Frame):
    def __init__(self, *args, **kwds):
        # begin wxGlade: MyBaseMotifs.__init__
        kwds["style"] = wx.CAPTION|wx.CLOSE_BOX|wx.MINIMIZE_BOX|wx.MAXIMIZE_BOX|wx.SYSTEM_MENU|wx.RESIZE_BORDER|wx.NO_BORDER|wx.CLIP_CHILDREN
        wx.Frame.__init__(self, *args, **kwds)
        self.lb_1 = wx.StaticText(self, -1, "SEARCH FOR MHC MOTIFS", style=wx.ALIGN_CENTRE)
        #self.fd = wx.Panel(self, -1, style=wx.SIMPLE_BORDER|wx.TAB_TRAVERSAL)
        self.fd = filebrowse.FileBrowseButton(self, -1, size=(450, -1), changeCallback = self.fbbCallback)
        self.lb_31 = wx.StaticText(self, -1, "ANCHOR AAs ", style=wx.ALIGN_RIGHT)
        self.tc_anchor = wx.TextCtrl(self, -1, "FYW")
        self.lb_32 = wx.StaticText(self, -1, "Size", style=wx.ALIGN_RIGHT)
        self.sp_size = wx.SpinCtrl(self, -1, "9", min=1, max=50)
        self.cbx_no_nested = wx.CheckBox(self, -1, "NonNested       ")
        self.bt_run = wx.Button(self, -1, "Search")
        self.lb_41 = wx.StaticText(self, -1, "PATTERN AAs", style=wx.ALIGN_RIGHT)
        self.tc_pattern = wx.TextCtrl(self, -1, "")
        self.lb_42 = wx.StaticText(self, -1, "Pos ", style=wx.ALIGN_RIGHT)
        self.sp_pos = wx.SpinCtrl(self, -1, "9", min=1, max=50)
        self.cbx_two_anchors = wx.CheckBox(self, -1, "TwoAnchors     ")
        self.bt_xls = wx.Button(self, -1, "Open Xls")
        self.tc_out = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE|wx.TE_READONLY)

        self.__set_properties()
        self.__do_layout()
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: MyBaseMotifs.__set_properties
        self.SetSize((480, 251))
        self.SetMinSize((450,180))  ##
        self.SetBackgroundColour(wx.Colour(255, 168, 0))
        self.lb_1.SetBackgroundColour(wx.Colour(173, 255, 0))
        self.fd.SetBackgroundColour(wx.Colour(255, 255, 0))
        self.tc_anchor.SetMinSize((80, 20))
        self.sp_size.SetMinSize((50, 20))
        self.bt_run.SetMinSize((-1, -1))
        self.bt_run.SetBackgroundColour(wx.Colour(40, 200, 0))
        self.tc_pattern.SetMinSize((80, 20))
        self.sp_pos.SetMinSize((50, 20))
        self.tc_out.SetMinSize((392, 100))
        # end wxGlade

    #noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: MyBaseMotifs.__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        sizer_3 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(self.lb_1, 0, wx.ALL|wx.EXPAND|wx.ALIGN_CENTER_VERTICAL, 5)
        sizer_1.Add(self.fd, 0, wx.EXPAND, 0)
        sizer_2.Add(self.lb_31, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_2.Add(self.tc_anchor, 0, 0, 0)
        sizer_2.Add((30, 20), 0, 0, 0)
        sizer_2.Add(self.lb_32, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_2.Add(self.sp_size, 0, 0, 0)
        sizer_2.Add((19, 20), 1, 0, 0)
        sizer_2.Add(self.cbx_no_nested, 0, wx.TOP, 5)
        sizer_2.Add(self.bt_run, 0, wx.RIGHT|wx.BOTTOM, 3)
        sizer_1.Add(sizer_2, 0, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 0)
        sizer_3.Add(self.lb_41, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_3.Add(self.tc_pattern, 0, 0, 0)
        sizer_3.Add((30, 20), 0, 0, 0)
        sizer_3.Add(self.lb_42, 0, wx.LEFT|wx.RIGHT|wx.ALIGN_CENTER_VERTICAL, 2)
        sizer_3.Add(self.sp_pos, 0, 0, 0)
        sizer_3.Add((95, 20), 1, 0, 0)
        sizer_3.Add(self.cbx_two_anchors, 0, wx.TOP, 5)
        sizer_3.Add(self.bt_xls, 0, wx.RIGHT, 3)
        sizer_1.Add(sizer_3, 0, wx.EXPAND|wx.ALIGN_CENTER_HORIZONTAL, 0)
        sizer_1.Add(self.tc_out, 1, wx.ALL|wx.EXPAND, 3)
        self.SetSizer(sizer_1)
        self.Layout()
        # end wxGlade
    
    def fbbCallback(self, evt):
        pass

# end of class MyBaseMotifs


class MyApp(wx.App):
    def OnInit(self):
        wx.InitAllImageHandlers()
        motifs = BaseMotifs(None, -1, "")
        self.SetTopWindow(motifs)
        motifs.Show()
        return 1

# end of class MyApp

if __name__ == "__main__":
    app = MyApp(0)
    app.MainLoop()
