#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# function used in setup.py for py2exe
import os

def get_images():
    current_dir = os.getcwd()
    alist = []
    for filename in os.listdir('AAS'):
        path = os.path.join(current_dir, 'AAS', filename)
        alist.append(path)

    return 'AAS', alist

if __name__ == '__main__':
    import matplotlib
    print get_images()
    for item in matplotlib.get_py2exe_datafiles():
        print item
