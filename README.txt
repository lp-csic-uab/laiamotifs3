﻿LAIAMOTIFS vs 1.1 (version september 16, 2011)

LAIAMOTIFS is a tool for analyzing peptide sets determining the frecuency
of the aminoacids in each position of the peptide sequences and looking
for MHC type motifs 


Table of contents
-----------------
1.- DEPENDENCIES
2.- INSTALLATION
3.- THE SEQUENCES INPUT FILE
4.- HOW TO SELECT SPECIFIC AA FRECUENCIES
5.- FEATURES AND CHANGELOG
6.- TODO
7.- CONTRIBUTE


1.- DEPENDENCIES
----------------
If you work with LAIAMOTIFS source you need Python installed (tested with Python
version 2.6.6) as well as the following dependencies (versions only indicative):
    xlwt 0.7.2
    PIL 1.1.6
    Numpy 1.4.1
    wxPython 2.8.11.0
    commons (from lpcsicuab googlecode repository)
    

2.- INSTALLATION
----------------
Tested *only* in windows XP

1a) Install LAIAMOTIFS FROM WINDOWS INSTALLER
    a.- Download LAIAMOTIFS_x.y_setup.exe installer from repository.
    b.- Double click on the installer in any directory.
    c.- Run LAIAMOTIFS by double-clicking on the LAIAMOTIFS direct access in your
        desktop or from the START-PROGRAMS application created by the installer.

1b) Install LAIAMOTIFS FROM SOURCE (when available)
    a.- Download LAIAMOTIFS and commons zip archives from repository
    b.- Unzip in python site-packages or in another folder in the python path
    c.- Run LAIAMOTIFS by double-clicking on the lmotifs.pyw module.

1c) Install LAIAMOTIFS FROM MERCURIAL
    a.- Get a copy of the LAIAMOTIFS and commons repositories from mercurial:
        http://code.google.com/p/lp-csic-uab/source/checkout?repo=LAIAMOTIFS
    b.- Locate de folders and run the application as indicated in 1b.


3.- THE SEQUENCES INPUT FILE
-------------------------------------
Laia Motifs analizes sequence sets that are provided in an input text file,
one sequence per line.
Comments can be inserted by commenting the line with the tag #.
A test sequence file mhc.txt is provided in the test folder.


4.- HOW TO SELECT SPECIFIC AA FRECUENCIES
-------------------------------------
LaiaMotifs correct the observed frecuencies of aminoacids with the naturally
observed frecuencies in vertebrates. This default frecuency data is taken from:
 "http://www.tiem.utk.edu/bioed//webmodules/aminoacids.htm"
            
- To use other frecuencies create a text file "frecuencias.txt" with lines of the type
  C:0.11 (aminoacid:frecuency).
- Use the tag 'name' at the first line in the file to indicate the data source
 (pe. name:frog).
- Start any line with the tag # for comments or to hide the data from the aminoacid
  in this line
- If an aminoacid is not indicated in the file or it is hidden (commented out), 
  its default frecuency in vertebrates will be used.


5.- FEATURES AND CHANGELOG
-----------------------
1.00 september 2011
- Initiate this record.
- Represent Leu/ILe with the correct code J (before was X)
  Use X exclusively for unknown aminoacids


6.- TODO
--------
- Make use of Viewer visible with... additional button?


7.- CONTRIBUTE
-----------
These programs are made to be useful. If you use them and don't work
entirely as you expect, let me know about features you think are needed, and
I'll try to include them in future releases.

Joaquín Abián September 2011